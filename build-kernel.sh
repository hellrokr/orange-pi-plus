#!/bin/bash -x
trap 'exit 88' ERR

THIS_SCRIPT_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
KERNELDIR=${KERNELDIR:-"$THIS_SCRIPT_DIR/linux-3.4"}

CROSS_COMPILE=${CROSS_COMPILE:-"arm-linux-gnueabi-"}
[[ $(uname -p) =~ armm ]] && unset CROSS_COMPILE

PROCS=$(grep '^processor' /proc/cpuinfo | wc -l)

make -j${PROCS} -C "$KERNELDIR" \
    ARCH=arm \
    CROSS_COMPILE=${CROSS_COMPILE} \
    uImage modules

make -j${PROCS} -C "$KERNELDIR" \
    ARCH=arm \
    CROSS_COMPILE=${CROSS_COMPILE} \
    INSTALL_MOD_PATH=output \
    modules_install firmware_install

cp "$KERNELDIR"/arch/arm/boot/uImage "$KERNELDIR"/output/uImage

echo success
