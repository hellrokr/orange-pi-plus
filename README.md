
This is the only linux tree that can be compiled for orange pi plus.

linux-3.4 from loboris tree (https://github.com/loboris/OrangePi-BuildLinux) where 
I applied http://moinejf.free.fr/opi2/kernel-3.4.patch and run 
'./build_linux.sh plus'. 

This gives me working and rebuildable kernel tree for orange pi plus.

To compile it on ubuntu non-arm machine set CROSS_COMPILE variable in 
build-kernel.sh. Unset it when compiling on arm.

config-* is config file I have used to get working uImage.